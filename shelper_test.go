package packages

import (
	"strings"
	"testing"

	"gitlab.com/soteapps/packages/shelper"
)

func TestContainsSpecialCharacters(t *testing.T) {
	
	if x := shelper.ContainsSpecialCharacters("myFieldName", "my`Field.Value"); x.ErrCode != 400060 {
		t.Errorf("Expected ErrCode : 400060 Returned ErrCode : %v , with FmtErrMsg : %v", x.ErrCode, x.FmtErrMsg)
		t.Fail()
	}
	
	t.Errorf(x.FmtErrMsg)

}

func TestContainsSpecialCharactersNone(t *testing.T) {
	if x := shelper.ContainsSpecialCharacters("myFieldName", "myFieldValue"); x.ErrCode != 0 {
		t.Errorf("Expected ErrCode : 0 Returned ErrCode : %v , with FmtErrMsg : %v", x.ErrCode, x.FmtErrMsg)
		t.Fail()
	}
	
	t.Errorf(x.FmtErrMsg)
}