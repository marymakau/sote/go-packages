package main

import (
	"fmt"
	"encoding/json"
	
	"gitlab.com/soteapps/packages/shelper" 
)

func PrettyPrint(v interface{})  {
	b, err := json.MarshalIndent(v, "", "  ")
	if err == nil {
		 fmt.Println(string(b))
	}
	return 
}

func main() {
	PrettyPrint(shelper.ContainsSpecialCharacters("myFieldName", `myFieldValue -`))

	 PrettyPrint(shelper.ContainsInvalidChars("myFieldName", `myFieldValue *`))

	 PrettyPrint(shelper.ContainsAlphaUnderscoreChars("myFieldName", `myFieldValue 123`))

	 PrettyPrint(shelper.ContainsAlphaSpaceChars("myFieldName", `myFieldValue 123`))

	 PrettyPrint(shelper.ContainsAlphaSpaceCommaChars("myFieldName", `myFieldValue 123`))

}