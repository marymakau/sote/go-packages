/*
	This is a wrapper for validation messages used by Sote GO software developers.

	The GO helper package should not be used directly.  This package sets the format of 
	the validation message so they are uniform.
*/

package shelper

import (
	"regexp"
	"fmt"
	"runtime"
	"gitlab.com/soteapps/packages/slogger"
	"gitlab.com/soteapps/packages/serror"
)

type soteRet struct {
	ErrCode          interface{}
	ErrType          string
	ParamCount       int
	ParamDescription string
	FmtErrMsg        string
	LUErrorDetails   map[string]string 
	Loc              string
}

func ErrLoc() string {
    pc := make([]uintptr, 10) 
    runtime.Callers(2, pc)
    f := runtime.FuncForPC(pc[0])
    return f.Name()
}

/**
 * Contains Special Characters will test if the following characters are in the string, then return error if true
 * !"#\$%&''()*+,-./:;<=>?@[\]^_`{|}~
 *
 * @param string $myFieldName
 * @param string $myFieldValue
 *
 * @return array Keys: ErrCode, ErrType, ParamCount, ParamDescription, FmtErrMsg, LUErrorDetails, Loc
 *               FmtErrMsg on an error will return special_characters (key) with the list of special characters (value)
 */


func ContainsSpecialCharacters(myFieldName string, myFieldValue string) soteRet {

	slogger.DebugMethod()

	var result soteRet

	var re = regexp.MustCompile(`(?m)[!"#\$%&\'\'\(\)\*\+,-\.\/:;<=>\?@\[\\\\\]\^_\x60\{\|}~]`)

	if(re.MatchString(myFieldValue)) {

		x :=  serror.GetSError(400060, []string{myFieldName , myFieldValue }, serror.EmptyMap)

		x.Loc = ErrLoc()

		x.LUErrorDetails = make(map[string]string)

		x.LUErrorDetails["special_characters"] = "!\"#\\$%&''()*+,-./:;<=>?@[\\]^_`{|}~"

		result = soteRet(x)
		
	} else { 

		result = soteRet{ 0, "Success", 0, "", "", make(map[string]string), ErrLoc()}
	}
	
	slogger.Debug("ErrCode: " + fmt.Sprint(result.ErrCode)+ " ErrType: " + result.ErrType + " FmtErrMsg: " + result.FmtErrMsg + " Loc: " + result.Loc)
	return result
}

/**
 * Contains Invalid Characters will test if the following characters are in the string, then return error if true
 * "\$%&''()*\/[\]^_`{|}~
 *
 * @param string $myFieldName
 * @param string $myFieldValue
 *
 * @return array Keys: ErrCode, ErrType, ParamCount, ParamDescription, FmtErrMsg, LUErrorDetails, Loc
 *               FmtErrMsg on an error will return invalid_characters (key) with the list of invalid characters (value)
 */


func ContainsInvalidChars(myFieldName string, myFieldValue string) soteRet {

	slogger.DebugMethod()

	var result soteRet

	var re = regexp.MustCompile(`(?m)["\%&\'\'\(\)\*\/\[\\\\\]\^_\x60\{\|}~]`)

	if(re.MatchString(myFieldValue)) {

		x :=  serror.GetSError(400060, []string{myFieldName , myFieldValue }, serror.EmptyMap)

		x.Loc = ErrLoc()

		x.LUErrorDetails = make(map[string]string)

		x.LUErrorDetails["invalid_characters"] = "\"\\$%&''()*/[\\]^_`{|}~"

		result = soteRet(x)
		
	} else { 

		result = soteRet{ 0, "Success", 0, "", "", make(map[string]string), ErrLoc()}
	}
	
	slogger.Debug("ErrCode: " + fmt.Sprint(result.ErrCode)+ " ErrType: " + result.ErrType + " FmtErrMsg: " + result.FmtErrMsg + " Loc: " + result.Loc)
	return result
}

/**
 * Contains Alpha Undersore Characters will test a string contains only alpha(case insensitive) and undescore, then return error if false
 *
 * @param string $myFieldName
 * @param string $myFieldValue
 *
 * @return array Keys: ErrCode, ErrType, ParamCount, ParamDescription, FmtErrMsg, LUErrorDetails, Loc
 *               FmtErrMsg on an error will return allowed_characters  (key) with the *A-Za-z_* regex (value)
 */


func ContainsAlphaUnderscoreChars(myFieldName string, myFieldValue string) soteRet {

	slogger.DebugMethod()

	var result soteRet

	var re = regexp.MustCompile(`(?m)^[a-zA-Z_]+$`)

	if(re.MatchString(myFieldValue)) {

		result = soteRet{ 0, "Success", 0, "", "", make(map[string]string), ErrLoc()}

		
	} else { 

		x :=  serror.GetSError(400065, []string{myFieldName , myFieldValue }, serror.EmptyMap)

		x.Loc = ErrLoc()

		x.LUErrorDetails = make(map[string]string)

		x.LUErrorDetails["allowed_characters"] = "a-zA-Z_"

		result = soteRet(x)
	}
	
	slogger.Debug("ErrCode: " + fmt.Sprint(result.ErrCode)+ " ErrType: " + result.ErrType + " FmtErrMsg: " + result.FmtErrMsg + " Loc: " + result.Loc)
	return result
}

/**
 * Contains Alpha Space Characters will test a string contains only alpha(case insensitive) and space, then return error if false
 *
 * @param string $myFieldName
 * @param string $myFieldValue
 *
 * @return array Keys: ErrCode, ErrType, ParamCount, ParamDescription, FmtErrMsg, LUErrorDetails, Loc
 *               FmtErrMsg on an error will return allowed_characters  (key) with the *A-Za-z_\s* regex (value)
 */


func ContainsAlphaSpaceChars(myFieldName string, myFieldValue string) soteRet {

	slogger.DebugMethod()

	var result soteRet

	var re = regexp.MustCompile(`(?m)^[a-zA-Z\s]+$`)

	if(re.MatchString(myFieldValue)) {

		result = soteRet{ 0, "Success", 0, "", "", make(map[string]string), ErrLoc()}

		
	} else { 

		x :=  serror.GetSError(400065, []string{myFieldName , myFieldValue }, serror.EmptyMap)

		x.Loc = ErrLoc()

		x.LUErrorDetails = make(map[string]string)

		x.LUErrorDetails["allowed_characters"] = "a-zA-Z\\s"

		result = soteRet(x)
	}
	
	slogger.Debug("ErrCode: " + fmt.Sprint(result.ErrCode)+ " ErrType: " + result.ErrType + " FmtErrMsg: " + result.FmtErrMsg + " Loc: " + result.Loc)
	return result
}

/**
 * Contains Alpha Space Comma Characters will test a string contains only alpha(case insensitive), space and comma, then return error if false
 *
 * @param string $myFieldName
 * @param string $myFieldValue
 *
 * @return array Keys: ErrCode, ErrType, ParamCount, ParamDescription, FmtErrMsg, LUErrorDetails, Loc
 *               FmtErrMsg on an error will return allowed_characters  (key) with the *A-Za-z_\s,* regex (value)
 */


func ContainsAlphaSpaceCommaChars(myFieldName string, myFieldValue string) soteRet {

	slogger.DebugMethod()

	var result soteRet

	var re = regexp.MustCompile(`(?m)^[a-zA-Z\s,]+$`)

	if(re.MatchString(myFieldValue)) {

		result = soteRet{ 0, "Success", 0, "", "", make(map[string]string), ErrLoc()}

		
	} else { 

		x :=  serror.GetSError(400065, []string{myFieldName , myFieldValue }, serror.EmptyMap)

		x.Loc = ErrLoc()

		x.LUErrorDetails = make(map[string]string)

		x.LUErrorDetails["allowed_characters"] = "a-zA-Z\\s,"

		result = soteRet(x)
	}
	
	slogger.Debug("ErrCode: " + fmt.Sprint(result.ErrCode)+ " ErrType: " + result.ErrType + " FmtErrMsg: " + result.FmtErrMsg + " Loc: " + result.Loc)
	return result
}